package at.markusgeyer.discover;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class popup extends AppCompatActivity {

    public int checked = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popup);


        Button ja = (Button)findViewById(R.id.button);
        Button nein = (Button)findViewById(R.id.button2);

        ja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checked = 1;
            }
        });

        nein.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checked = 0;
            }
        });
    }
}
