package at.markusgeyer.discover;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import static at.markusgeyer.discover.Dashboard.justifyListViewHeightBasedOnChildren;

/*
    Markus Geyer am 14.08.2018
 */

public class Team extends AppCompatActivity {

    public static final String TAG = "StepCounter/Team";

    private FirebaseAuth firebaseAuth;
    private ListView mostactive,mostSteps;
    private TextView Teamname, Teamactivities;

    int i = 0, activitycount = 0, count = 0 ,step=0, steprank = 0;
    private Bitmap bitmap;

    private SwipeRefreshLayout SwipeRefresh;

    private FirebaseFirestore db;
    private FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseStorage storage = FirebaseStorage.getInstance();


    private FirebaseFirestore firestore = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        SwipeRefresh = (SwipeRefreshLayout) findViewById(R.id.SwipeRefresh);
        mostactive = (ListView) findViewById(R.id.teamactivity);
        mostSteps = (ListView)findViewById(R.id.teamsteps);
        Teamname = (TextView)findViewById(R.id.Teamname);
        Teamactivities = (TextView)findViewById(R.id.Teamactivities);


        db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, Login.class));
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_dashboard);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Einstellungen.BottomNavigationViewHelper.disableShiftMode(navigation);

        refresh();
    }


    private void refresh(){
        // ---------------------ausgelöst mittels runterswipen - aktualisiert das komplette Team-----------------------------------------------------
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh2,R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        activitydata();
                        stepdata();
                        Teamname.setText("5AHEL");
                        swipeRefreshLayout.setRefreshing(false);
                        checkConnetion();
                        i = i +1;
                    }
                },i);
            }
        });
        // ------------------------------------------------------------------------------------------------------------------------------------------------

        swipeRefreshLayout.setRefreshing(true);
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                activitydata();
                stepdata();
                Teamname.setText("5AHEL");
                swipeRefreshLayout.setRefreshing(false);
                checkConnetion();
                i = i +1;
            }
        },i);
    }

    private void checkConnetion(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else {
            Toast.makeText(this, "Überprüfe deine Internetverbindung!", Toast.LENGTH_SHORT).show();
            connected = false;
        }
    }

    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);

    }

    private void activitydata(){
        count = 0;
        final ArrayList<mostActiveTeam> mostActiveTeamArrayList = new ArrayList<>();
        db.collection("5AHEL").orderBy("weeklyActivity", Query.Direction.DESCENDING).limit(3)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                count++;
                                i = (Integer.parseInt(""+document.get("weeklyActivity")));
                                activitycount = i + activitycount;
                                Teamactivities.setText("" +activitycount+" Aktivitäten diese Woche");

                                //Aktivitäten
                                mostActiveTeam first = new mostActiveTeam(""+document.get("vorname")+ " " + document.get("nachname"),document.getId(),count,i);
                                //Personen der Liste hinzufügen
                                mostActiveTeamArrayList.add(first);
                                mostActiveListAdapter adapter = new mostActiveListAdapter(Team.this, R.layout.mostactive_view,mostActiveTeamArrayList);
                                mostactive.setAdapter(adapter);
                                justifyListViewHeightBasedOnChildren(mostactive); //verhindert scrollen der Listview in urspünglicher ScrollViewv
                            }
                            //Collections.reverse(mostActiveTeamArrayList);

                        } else {
                            Teamactivities.setText("" +0+" Aktivitäten diese Woche");
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        count = 0;
        i=0;
        activitycount = 0;
    }

    private void stepdata(){
        steprank = 0;
        final SimpleDateFormat dateFormatStep = new SimpleDateFormat("yyyy.MM.dd");
        final Calendar calendar = Calendar.getInstance();
        final ArrayList<mostStepTeam> mostStepTeamArrayList = new ArrayList<>();
        db.collection("5AHEL").orderBy("schritte", Query.Direction.DESCENDING).limit(3)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                steprank++;
                                step = (Integer.parseInt(""+document.get("schritte")));

                                if((""+document.get("lastonline")).equals(""+dateFormatStep.format(calendar.getTime()))){
                                    //Schritte
                                    mostStepTeam erster = new mostStepTeam(""+document.get("vorname")+" "+document.get("nachname"),document.getId(),steprank,step);
                                    //Personen der Liste hinzufügen
                                    mostStepTeamArrayList.add(erster);
                                    mostStepListAdapter adapter = new mostStepListAdapter(Team.this, R.layout.moststep_view,mostStepTeamArrayList);
                                    mostSteps.setAdapter(adapter);
                                }
                                justifyListViewHeightBasedOnChildren(mostSteps); //verhindert scrollen der Listview in urspünglicher ScrollViewv
                            }
                            //Collections.reverse(mostActiveTeamArrayList);

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        steprank = 0;
        i=0;
        activitycount = 0;
    }


        private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent home = new Intent(Team.this, Dashboard.class);
                        startActivity(home);
                        overridePendingTransition(0,0);
                        home.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        return true;
                    case R.id.navigation_dashboard:
                        return true;
                    case R.id.navigation_notifications:
                        Intent badge = new Intent(Team.this, Erfolge.class);
                        startActivity(badge);
                        overridePendingTransition(0,0);
                        badge.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        return true;
                    case R.id.navigation_setting:
                        Intent setting = new Intent(Team.this, Einstellungen.class);
                        startActivity(setting);
                        overridePendingTransition(0,0);
                        setting.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        return true;
                }
                return false;
            }
        };

}
