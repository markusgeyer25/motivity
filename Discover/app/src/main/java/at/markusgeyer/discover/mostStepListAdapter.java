package at.markusgeyer.discover;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

public class mostStepListAdapter extends ArrayAdapter<mostStepTeam> {


    private FirebaseStorage storage = com.google.firebase.storage.FirebaseStorage.getInstance();
    private static final String TAG = "StepCounter";
    private Context Context;
    int ressource;

    public mostStepListAdapter(Context context, int resource, ArrayList<mostStepTeam> objects) {
        super(context, resource, objects);
        Context = context;
        ressource = resource;
    }

    @SuppressLint("ViewHolder")
    @NonNull
    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        //get Activity Data
        String name = getItem(position).getName();
        String picture = getItem(position).getPicture();
        int number = getItem(position).getNumber();
        int activitynumber = getItem(position).getSteps();

        //Acitvity erstellen
        mostStepTeam mostStepTeam = new mostStepTeam(name,picture,number,activitynumber);

        LayoutInflater inflater = LayoutInflater.from(Context);
        convertView = inflater.inflate(ressource,parent,false);

        TextView profnumber = (TextView)convertView.findViewById(R.id.number2);
        profnumber.setText(number+".");
        final ImageView activityView = (ImageView) convertView.findViewById(R.id.profpic);
        //Firebase Profilbild bekommen
        StorageReference imagedata = storage.getReference("firememes/"+picture+".png");
        final long ONE_MEGABYTE = 1024 * 1024;
        imagedata.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                activityView.setImageBitmap(bitmap);
            }
        });
        TextView profname = (TextView)convertView.findViewById(R.id.profnam);
        profname.setText(name);
        TextView numberofactivities = (TextView)convertView.findViewById(R.id.numberOfSteps);
        numberofactivities.setText(activitynumber+" Schritte");

        return convertView;
    }
}
