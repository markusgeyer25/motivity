package at.markusgeyer.discover;

public class listActivities {

    private String activity,date;
    int picture;

    public listActivities(String aktivitaet,String date,int drawable){
        this.activity = aktivitaet;
        this.date = date;
        this.picture = drawable;
    }

    public String getAktivitaet() {
        return activity;
    }

    public void setAktivitaet(String aktivitaet) {
        this.activity = aktivitaet;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getPicutre(){
        return picture;
    }

    public void setPicutre(int drawable){
        this.picture = drawable;
    }


}
