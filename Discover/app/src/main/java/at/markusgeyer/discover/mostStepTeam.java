package at.markusgeyer.discover;

public class mostStepTeam {

    private String name,picture;
    private int number,stepdata;

    public mostStepTeam(String name, String drawable, int row, int steps){
        this.name = name;
        this.picture = drawable;
        this.number = row;
        this.stepdata = steps;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getSteps() {
        return stepdata;
    }

    public void setSteps(int steps) {
        this.stepdata = steps;
    }
}
