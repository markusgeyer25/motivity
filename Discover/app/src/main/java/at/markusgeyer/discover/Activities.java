package at.markusgeyer.discover;

public class Activities {

    private String activity;
    private int picture;

    public Activities(String aktivitaet, int drawable){
        this.activity = aktivitaet;
        this.picture = drawable;
    }

    public String getAktivitaet() {
        return activity;
    }

    public void setAktivitaet(String aktivitaet) {
        this.activity = aktivitaet;
    }

    public int getPicutre(){
        return picture;
    }

    public void setPicutre(int drawable){
        this.picture = drawable;
    }

}
