package at.markusgeyer.discover;

import android.graphics.Bitmap;

public class mostActiveTeam {

    private String name;
    private int number,activitynumber;
    private String picture;

    public mostActiveTeam(String name, String drawable, int row, int numberofactivities){
        this.name = name;
        this.picture = drawable;
        this.number = row;
        this.activitynumber = numberofactivities;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getActivitynumber() {
        return activitynumber;
    }

    public void setActivitynumber(int activitynumber) {
        this.activitynumber = activitynumber;
    }
}
