package at.markusgeyer.discover;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

public class listAdapter extends ArrayAdapter<listActivities> {

    private static final String TAG = "StepCounter";
    private Context Context;
    int ressource;

    public listAdapter(Context context, int resource, ArrayList<listActivities> objects) {
        super(context, resource, objects);
        Context = context;
        ressource = resource;
    }

    @NonNull
    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        //get Activity Data
        String activity = getItem(position).getAktivitaet();
        String date = getItem(position).getDate();
        int picture = getItem(position).getPicutre();

        //Acitvity erstellen
        listActivities activities = new listActivities(activity,date,picture);

        LayoutInflater inflater = LayoutInflater.from(Context);
        convertView = inflater.inflate(ressource,parent,false);

        TextView activityText = (TextView)convertView.findViewById(R.id.doneactivityListe);
        activityText.setText(activity);
        TextView activityDatum = (TextView)convertView.findViewById(R.id.TextViewdone);
        String[] dateA = date.split(" ");
        activityDatum.setText(dateA[0]);
        ImageView activityView = (ImageView) convertView.findViewById(R.id.doneactivityView);
        activityView.setImageResource(picture);

        return convertView;
    }
}
