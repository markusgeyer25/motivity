package at.markusgeyer.discover;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.util.Log;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldPath;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;



import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.google.android.gms.fitness.data.Field.FIELD_STEPS;
import static com.google.common.base.Predicates.or;
import static java.text.DateFormat.getDateInstance;
import static java.text.DateFormat.getTimeInstance;
/*
    Markus Geyer 14.08.2018
 */

public class Dashboard extends AppCompatActivity {


    public GoogleApiClient mGoogleApiClient;
    public static final String TAG = "StepCounter/Dashboard";
    private static final int REQUEST_OAUTH_REQUEST_CODE = 0x1001;
    private ObjectAnimator progressAnimator,progressAnimator1,progressAnimator2,progressAnimator3,progressAnimator4,progressAnimator5,progressAnimator6;

    private int newZiel = 10000;

    private ListView activitylist;
    private FirebaseFirestore db;
    private FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    DataReadRequest readRequest;
    ArrayList<listActivities> listArrayList = new ArrayList<>();


    //-------------------------------------------Variablen und GUI Initialisierung-----------------------------------------------------------------
    ConstraintLayout setting, aktivities;
    ScrollView scrolly;
    RelativeLayout relativeLayout1,relativeLayout2,relativeLayout3,relativeLayout4,relativeLayout5;
    TextView usermail,Steps, textminusSteps, Activities, beforeCount, afterCount, textViewWeek1,textViewWeek2,textViewWeek3,textViewWeek4,textViewWeek5,textViewWeek6,textViewWeek7,
             DayOne,DayTwo, DayThree, DayFour, DayFive, DaySix, DaySeven;
    ProgressBar Stepprogressbar, Goalstepprogress, minusStepbar,weekStepData7,weekStepData6,weekStepData5,weekStepData4,weekStepData3,weekStepData2,weekStepData1;

    LottieAnimationView mario1,mario2,mario3,mario4,mario5,mario6,mario7;

    FloatingActionButton fab, fab1, fab2;
    LinearLayout fabLayout1, fabLayout2;
    View fabBGLayout;
    private boolean isFABOpen=false;

    int activcount = 0;
    int week = 0;
    int anzahl = 0;
    int steps = 0;
    int activity = 0, i = 0;
    int activitätsident = 0;
    int help1 = 0;

    Handler mHandler = new Handler();



    private FirebaseAuth firebaseAuth;
    private FirebaseUser currentFirebaseUser;



    // --------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);



        // ------------------------Verbindung mit Firebase Datenbank zur Authentifizierung-----------------------------------------------------------
        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null){
        }
        else{
            Intent login = new Intent(Dashboard.this, Login.class);
            startActivity(login);
            overridePendingTransition(0, 0);
            login.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        }
        // --------------------------------------------------------------------------------------------------------------------------------------------
        //------------------------------------------Firebase Firestore-------------------------------------------------------------------------------
        db = FirebaseFirestore.getInstance();
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);



        // ---------------------------Erstellen eines Objekts mit Google Fit Verknüpfung------------------------------------------------------------
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Fitness.HISTORY_API)
                .addApi(Fitness.RECORDING_API)
                .addApi(Fitness.SENSORS_API)
                .useDefaultAccount()
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                .build();
        mGoogleApiClient.connect();


        FitnessOptions fitnessOptions =
                FitnessOptions.builder()
                        .addDataType(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                        .addDataType(DataType.TYPE_STEP_COUNT_DELTA)
                        .build();
        // -------------------------------------------------------------------------------------------------------------------------------------------


        // ----------------------Identifizierung der verschiedenen GUI Objekte mittels ID---------------------------------------------------------------
        Steps = (TextView) findViewById(R.id.Steps);
        mTextMessage = (TextView) findViewById(R.id.message);
        textminusSteps = (TextView) findViewById(R.id.textminusSteps);
        Stepprogressbar = (ProgressBar) findViewById(R.id.Stepprogressbar);


        activitylist = (ListView) findViewById(R.id.doneactivitylist);


        setting = (ConstraintLayout) findViewById(R.id.container);
        aktivities = (ConstraintLayout) findViewById(R.id.activities);
        usermail = (TextView) findViewById(R.id.usermail);
        minusStepbar = (ProgressBar) findViewById(R.id.minusStepbar);
        Goalstepprogress = (ProgressBar) findViewById(R.id.Goalprogressbar);
        Activities = (TextView) findViewById(R.id.WeeklyActivities);

        scrolly = (ScrollView) findViewById(R.id.scrollDash);

        mario1 = (LottieAnimationView) findViewById(R.id.mario1);
        mario2 = (LottieAnimationView) findViewById(R.id.mario2);
        mario3 = (LottieAnimationView) findViewById(R.id.mario3);
        mario4 = (LottieAnimationView) findViewById(R.id.mario4);
        mario5 = (LottieAnimationView) findViewById(R.id.mario5);
        mario6 = (LottieAnimationView) findViewById(R.id.mario6);
        mario7 = (LottieAnimationView) findViewById(R.id.mario7);

        mario7.setVisibility(View.GONE);
        mario6.setVisibility(View.GONE);
        mario5.setVisibility(View.GONE);
        mario4.setVisibility(View.GONE);
        mario3.setVisibility(View.GONE);
        mario2.setVisibility(View.GONE);
        mario1.setVisibility(View.GONE);


        beforeCount = (TextView) findViewById(R.id.textbeforeCount);
        afterCount = (TextView) findViewById(R.id.textafterCount);

        weekStepData7 = (ProgressBar) findViewById(R.id.weekStepData7);
        weekStepData6 = (ProgressBar) findViewById(R.id.weekStepData6);
        weekStepData5= (ProgressBar) findViewById(R.id.weekStepData5);
        weekStepData4 = (ProgressBar) findViewById(R.id.weekStepData4);
        weekStepData3 = (ProgressBar) findViewById(R.id.weekStepData3);
        weekStepData2 = (ProgressBar) findViewById(R.id.weekStepData2);
        weekStepData1 = (ProgressBar) findViewById(R.id.weekStepData1);

        textViewWeek1 = (TextView) findViewById(R.id.textViewWeek1);
        textViewWeek2 = (TextView) findViewById(R.id.textViewWeek2);
        textViewWeek3 = (TextView) findViewById(R.id.textViewWeek3);
        textViewWeek4 = (TextView) findViewById(R.id.textViewWeek4);
        textViewWeek5 = (TextView) findViewById(R.id.textViewWeek5);
        textViewWeek6 = (TextView) findViewById(R.id.textViewWeek6);
        textViewWeek7 = (TextView) findViewById(R.id.textViewWeek7);

        DayOne = (TextView)findViewById(R.id.DayOne);
        DayTwo = (TextView)findViewById(R.id.DayTwo);
        DayThree = (TextView)findViewById(R.id.DayThree);
        DayFour = (TextView)findViewById(R.id.DayFour);
        DayFive = (TextView)findViewById(R.id.DayFive);
        DaySix = (TextView)findViewById(R.id.DaySix);
        DaySeven = (TextView) findViewById(R.id.DaySeven);


        final Button ja = (Button)findViewById(R.id.ja);
        final Button nein = (Button)findViewById(R.id.nein);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_home);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Einstellungen.BottomNavigationViewHelper.disableShiftMode(navigation);


        minusStepbar.setMax(10000);
        minusStepbar.setProgress(10000);
        Goalstepprogress.setVisibility(View.INVISIBLE);
        Steps.setText("0");

        checkConnetion();

        //--------------------------------FloatingActionButton definieren ----------------------------------------------------------------------
        fabLayout1= (LinearLayout) findViewById(R.id.fabLayout1);
        fabLayout2= (LinearLayout) findViewById(R.id.fabLayout2);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2= (FloatingActionButton) findViewById(R.id.fab2);
        fabBGLayout=findViewById(R.id.fabBGLayout);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isFABOpen){
                    showFABMenu();
                }else{
                    closeFABMenu();
                }
            }
        });

        if(isFABOpen){
            closeFABMenu();
        }

        fabBGLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeFABMenu();
            }
        });



        // -----------------------------Ueberpruefung ob Google Fit Erlaubnis erteilt wurde-------------------------------------------------------------
        if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(this), fitnessOptions)) {
            GoogleSignIn.requestPermissions(
                    this,
                    REQUEST_OAUTH_REQUEST_CODE,
                    GoogleSignIn.getLastSignedInAccount(this),
                    fitnessOptions);
        } else {
            //wenn keine Erlaubnis, wird um Erlaubnis gebeten
            subscribe();
        }
        // --------------------------------------------------------------------------------------------------------------------------------------------


        // ---------------------ausgelöst mittels runterswipen - aktualisiert das komplette Dashboard-----------------------------------------------------
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.SwipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.refresh1,R.color.refresh1,R.color.refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false);
                        activity = 0;
                        i = i +2;
                        refresh();
                        //findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                        }
                },i);
            }
        });
        // ------------------------------------------------------------------------------------------------------------------------------------------------

        //------------------------------------------------Aktivitäten löschen---------------------------------------------------------------------------
        activitylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                final ConstraintLayout popup = (ConstraintLayout)findViewById(R.id.poplayout);
                popup.setVisibility(View.VISIBLE);

                final long help = id;
                ja.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Aktivitäten").document(""+listArrayList.get((int)help).getDate())
                                .delete();
                        if(help == 0){
                            Activities.setText("0 Aktivitäten diese Woche");
                        }
                        if(listArrayList.size() == 0){
                            aktivities.setVisibility(View.GONE);
                        }
                        listArrayList.clear();
                        activityread();
                        popup.setVisibility(View.INVISIBLE);
                    }
                });

                nein.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        popup.setVisibility(View.INVISIBLE);
                    }
                });

            }
        });

       //-----------------------------------------Schrittziel abfragen------------------------------------------------------------------
        textminusSteps.setText(""+((Integer.parseInt(""+Stepprogressbar.getMax()))-(Integer.parseInt(""+Stepprogressbar.getProgress()))));
        newZiel = Integer.parseInt(""+Stepprogressbar.getMax());
    }
    private void checkConnetion(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else {
            Toast.makeText(this, "Überprüfe deine Internetverbindung!", Toast.LENGTH_SHORT).show();
            connected = false;
        }
    }

    // ----------------------------------------Aktivitäten anzeigen-----------------------------------------------------------------------

    private void activityread() {

        final SimpleDateFormat dateFormatActivity = new SimpleDateFormat("yyyy.MM.dd");
        final SimpleDateFormat dateFormatActivityCollection = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss");
        final Calendar ca = Calendar.getInstance();
        ca.add(Calendar.DAY_OF_WEEK, -7);

        activcount = 0;
        anzahl = 0;
        listArrayList.clear();
            db.collection("5AHEL").document("" + currentFirebaseUser.getUid()).collection("Aktivitäten").orderBy("date", Query.Direction.ASCENDING).whereGreaterThan("date",dateFormatActivityCollection.format(ca.getTime()))
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                    String dateID = ""+document.getId();
                                    Log.e(TAG, dateID);




                                    anzahl = anzahl + 1;

                                        String help = "" + document.get("picutre");
                                        int drawable = Integer.parseInt(help);
                                        listActivities activity = new listActivities("" + document.get("aktivitaet"), "" + dateID, drawable);
                                        listArrayList.add(activity);
                                        ListAdapter adapter = new listAdapter(Dashboard.this, R.layout.done_activity_view, listArrayList);
                                        activitylist.setAdapter(adapter);
                                        justifyListViewHeightBasedOnChildren(activitylist); //verhindert scrollen der Listview in urspünglicher ScrollView

                                    Map<String, Object> activ = new HashMap<>();
                                    activ.put("weeklyActivity", anzahl);

                                    if (anzahl == 1) {
                                        Activities.setText("" + anzahl + " Aktivität diese Woche");
                                    } else {
                                        Activities.setText("" + anzahl + " Aktivitäten diese Woche");
                                    }
                                    db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).set(activ,SetOptions.merge());


                                }
                                Collections.reverse(listArrayList);
                                scrolly.smoothScrollTo(0, 0);//scrollt nach ganz oben

                            } else {
                                Activities.setText("" + 0 + " Aktivitäten diese Woche");
                                Log.d(TAG, "Error getting documents: ", task.getException());
                            }
                        }
                    });
            anzahl = 0;
    }
    //--------------------------------------------Disable scrolling in ListView--------------------------------------------------------------------
    public static void justifyListViewHeightBasedOnChildren (ListView listView) {

        ListAdapter adapter = listView.getAdapter();

        if (adapter == null) {
            return;
        }
        ViewGroup vg = listView;
        int totalHeight = 0;
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, vg);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams par = listView.getLayoutParams();
        par.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(par);
        listView.requestLayout();
    }

    //--------------------------------------------FloatingActionButton hilfe ----------------------------------------------------------------------
    private void showFABMenu(){
        isFABOpen=true;
        fabLayout1.setVisibility(View.VISIBLE);
        fabLayout2.setVisibility(View.VISIBLE);
        fabBGLayout.setVisibility(View.VISIBLE);

        fab.animate().rotationBy(135);
        fabLayout1.animate().translationY(-getResources().getDimension(R.dimen.standard_55));
        fabLayout2.animate().translationY(-getResources().getDimension(R.dimen.standard_100));

        fab1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent setting = new Intent(Dashboard.this, Add.class);
                startActivity(setting);
                overridePendingTransition(0,0);
                setting.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            }
        });
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent change = new Intent(Dashboard.this, ChangeGoal.class);
                startActivity(change);
                overridePendingTransition(0,0);
                change.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            }
        });
    }

    private void closeFABMenu(){
        isFABOpen=false;
        fabBGLayout.setVisibility(View.GONE);
        fab.animate().rotationBy(-45);
        fabLayout1.animate().translationY(0);
        fabLayout2.animate().translationY(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if(!isFABOpen){
                    fabLayout1.setVisibility(View.GONE);
                    fabLayout2.setVisibility(View.GONE);
                }

            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }
    //--------------------------------FloatingActionButton definieren -----------------------------------------------------------------------------


    // ---------------------------------------Verhindert das Zurück klicken, da es keine Subpages gibt-------------------------------------------------------
    public void onBackPressed(){
        if(!isFABOpen){
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }else{
            closeFABMenu();
        }
    }
    // -------------------------------------------------------------------------------------------------------------------------------------------------------

    private void refresh(){
        aktivities.setVisibility(View.VISIBLE);
        setGoal();
        activityread();
        readStepData();
        checkConnetion();
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
    }

    // --------------------------------Überprüft die Erlaubniss von Google Fit----------------------------------------------------------------------------
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_OAUTH_REQUEST_CODE) {
                //sollte die Erlaubnis "ok" sein, so werden die Schritte abgerufen
                subscribe();
            }
        }
    }
    // ----------------------------------------------------------------------------------------------------------------------------------------------------


    // --------------------------------Abfrage der Schritte für den eingeloggten Google Fit Account--------------------------------------------------------
    public void subscribe() {

        Fitness.getRecordingClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .subscribe(DataType.TYPE_STEP_COUNT_CUMULATIVE)
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    //schaut ob Zugriff auf Fitnessdaten erlaubt wurde, wenn ja werden Schritte und Aktivitäten gelesen
                                    Log.i(TAG, "Erfolgreich angemeldet");
                                    refresh();

                                } else {
                                    //wenn keine Erlaubnis, wird noch einmal gefragt
                                    Log.w(TAG, "Anmeldung konnte nicht abgeschlossen werden.", task.getException());
                                    subscribe();
                                }
                               // findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                            }
                        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------


    // --------------------------------Schritte werden aus Google Fit ausgelesen und auf ProgressBar grafisch dargestellt-------------------------------------
    private void readStepData() {
        Fitness.getHistoryClient(this, GoogleSignIn.getLastSignedInAccount(this))
                .readDailyTotal(DataType.TYPE_STEP_COUNT_DELTA)
                .addOnSuccessListener(
                        new OnSuccessListener<DataSet>() {
                            @Override
                            public void onSuccess(DataSet dataSet) {
                                final Drawable reached = getResources().getDrawable(R.drawable.reached);
                                int total =
                                        dataSet.isEmpty()
                                                ? 0
                                                : dataSet.getDataPoints().get(0).getValue(FIELD_STEPS).asInt();

                                SimpleDateFormat dateFormatStep = new SimpleDateFormat("yyyy.MM.dd");
                                SimpleDateFormat dateFormatStepDate = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss");
                                final Calendar calendar = Calendar.getInstance();

                                Map<String, Object> lastonline = new HashMap<>();
                                lastonline.put("lastonline", ""+dateFormatStep.format(calendar.getTime()));

                                Map<String, Object> step = new HashMap<>();
                                step.put("schritte", total);

                                Map<String, Object> stepdate = new HashMap<>();
                                stepdate.put("date", dateFormatStepDate.format(calendar.getTime()));

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
                                final SimpleDateFormat dateFormatday = new SimpleDateFormat("EEE");

                                db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat.format(calendar.getTime()))
                                        .set(step);


                                db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).set(step,SetOptions.merge());
                                db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).set(lastonline,SetOptions.merge());

                                Steps.setText("" +total);
                                int minus = newZiel;
                                    Stepprogressbar.setProgress((int)total);
                                    weekStepData7.setProgress((int)total);


                                DaySeven.setText(""+dateFormatday.format(calendar.getTime()));
                                textViewWeek7.setText(""+total);
                                progressAnimator = ObjectAnimator.ofInt(weekStepData7,"Progress",0,weekStepData7.getProgress());
                                progressAnimator.setDuration(weekStepData7.getProgress());
                                progressAnimator.start();
                                if(total >= newZiel)
                                {
                                    weekStepData7.setProgressDrawable(reached);
                                    progressAnimator.setDuration(weekStepData7.getProgress());
                                    progressAnimator.start();
                                    mario7.setVisibility(View.VISIBLE);
                                    mario7.playAnimation();
                                }

                                String fmt = "yyyy.MM.dd";
                                String date = dateFormat.format(calendar.getTime());
                                DateFormat dateFormat1 = new SimpleDateFormat(fmt);


                                try {
                                    calendar.setTime(dateFormat1.parse(date));
                                    calendar.add(Calendar.DAY_OF_MONTH, -1);
                                    DaySix.setText(""+dateFormatday.format(calendar.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG,""+dateFormat1.format(calendar.getTime()));




                                DocumentReference docRef6 = db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat1.format(calendar.getTime()));
                                docRef6.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                String time = ""+document.getData();
                                                String[] date6 = time.split("=");
                                                String help = date6[1];
                                                help = help.substring(0,help.length()-1);

                                                Log.e(TAG,""+help);


                                                weekStepData6.setProgress(Integer.parseInt(""+help));
                                                textViewWeek6.setText(""+help);
                                                progressAnimator6 = ObjectAnimator.ofInt(weekStepData6,"Progress",0,weekStepData6.getProgress());
                                                progressAnimator6.setDuration(weekStepData6.getProgress());
                                                progressAnimator6.start();
                                                if(Integer.parseInt(""+help) >= newZiel)
                                                {
                                                    weekStepData6.setProgressDrawable(reached);
                                                    progressAnimator6.setDuration(weekStepData6.getProgress());
                                                    progressAnimator6.start();
                                                    mario6.setVisibility(View.VISIBLE);
                                                    mario6.playAnimation();

                                                }
                                            } else {
                                                textViewWeek6.setText("0");
                                                Log.d(TAG, "No such document");
                                            }
                                        } else {
                                            Log.d(TAG, "get failed with ", task.getException());
                                        }
                                    }
                                });


                                try {
                                    calendar.setTime(dateFormat1.parse(date));
                                    calendar.add(Calendar.DAY_OF_MONTH, -2);
                                    DayFive.setText(""+dateFormatday.format(calendar.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG,""+dateFormat1.format(calendar.getTime()));



                                DocumentReference docRef5 = db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat1.format(calendar.getTime()));
                                docRef5.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                String time = ""+document.getData();
                                                String[] date6 = time.split("=");
                                                String help = date6[1];
                                                help = help.substring(0,help.length()-1);

                                                Log.e(TAG,""+help);


                                                weekStepData5.setProgress(Integer.parseInt(""+help));
                                                textViewWeek5.setText(""+help);
                                                progressAnimator5 = ObjectAnimator.ofInt(weekStepData5,"Progress",0,weekStepData5.getProgress());
                                                progressAnimator5.setDuration(weekStepData5.getProgress());
                                                progressAnimator5.start();
                                                if(Integer.parseInt(""+help) >= newZiel)
                                                {
                                                    weekStepData5.setProgressDrawable(reached);
                                                    progressAnimator5.setDuration(weekStepData5.getProgress());
                                                    progressAnimator5.start();
                                                    mario5.setVisibility(View.VISIBLE);
                                                    mario5.playAnimation();
                                                }
                                            } else {
                                                textViewWeek5.setText("0");
                                                Log.d(TAG, "No such document");
                                            }
                                        } else {
                                            Log.d(TAG, "get failed with ", task.getException());
                                        }
                                    }
                                });

                                try {
                                    calendar.setTime(dateFormat1.parse(date));
                                    calendar.add(Calendar.DAY_OF_MONTH, -3);
                                    DayFour.setText(""+dateFormatday.format(calendar.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG,""+dateFormat1.format(calendar.getTime()));



                                DocumentReference docRef4 = db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat1.format(calendar.getTime()));
                                docRef4.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                String time = ""+document.getData();
                                                String[] date6 = time.split("=");
                                                String help = date6[1];
                                                help = help.substring(0,help.length()-1);

                                                Log.e(TAG,""+help);


                                                weekStepData4.setProgress(Integer.parseInt(""+help));
                                                textViewWeek4.setText(""+help);
                                                progressAnimator4 = ObjectAnimator.ofInt(weekStepData4,"Progress",0,weekStepData4.getProgress());
                                                progressAnimator4.setDuration(weekStepData4.getProgress());
                                                progressAnimator4.start();
                                                if(Integer.parseInt(""+help) >= newZiel)
                                                {
                                                    weekStepData4.setProgressDrawable(reached);
                                                    progressAnimator4.setDuration(weekStepData4.getProgress());
                                                    progressAnimator4.start();
                                                    mario4.setVisibility(View.VISIBLE);
                                                    mario4.playAnimation();
                                                }
                                            } else {
                                                textViewWeek4.setText("0");
                                                Log.d(TAG, "No such document");
                                            }
                                        } else {
                                            Log.d(TAG, "get failed with ", task.getException());
                                        }
                                    }
                                });

                                try {
                                    calendar.setTime(dateFormat1.parse(date));
                                    calendar.add(Calendar.DAY_OF_MONTH, -4);
                                    DayThree.setText(""+dateFormatday.format(calendar.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG,""+dateFormat1.format(calendar.getTime()));



                                DocumentReference docRef3 = db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat1.format(calendar.getTime()));
                                docRef3.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                String time = ""+document.getData();
                                                String[] date6 = time.split("=");
                                                String help = date6[1];
                                                help = help.substring(0,help.length()-1);

                                                Log.e(TAG,""+help);


                                                weekStepData3.setProgress(Integer.parseInt(""+help));
                                                textViewWeek3.setText(""+help);
                                                progressAnimator3 = ObjectAnimator.ofInt(weekStepData3,"Progress",0,weekStepData3.getProgress());
                                                progressAnimator3.setDuration(weekStepData3.getProgress());
                                                progressAnimator3.start();
                                                if(Integer.parseInt(""+help) >= newZiel)
                                                {
                                                    weekStepData3.setProgressDrawable(reached);
                                                    progressAnimator3.setDuration(weekStepData3.getProgress());
                                                    progressAnimator3.start();
                                                    mario3.setVisibility(View.VISIBLE);
                                                    mario3.playAnimation();
                                                }
                                            } else {
                                                textViewWeek3.setText("0");
                                                Log.d(TAG, "No such document");
                                            }
                                        } else {
                                            Log.d(TAG, "get failed with ", task.getException());
                                        }
                                    }
                                });

                                try {
                                    calendar.setTime(dateFormat1.parse(date));
                                    calendar.add(Calendar.DAY_OF_MONTH, -5);
                                    DayTwo.setText(""+dateFormatday.format(calendar.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG,""+dateFormat1.format(calendar.getTime()));



                                DocumentReference docRef2 = db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat1.format(calendar.getTime()));
                                docRef2.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                String time = ""+document.getData();
                                                String[] date6 = time.split("=");
                                                String help = date6[1];
                                                help = help.substring(0,help.length()-1);

                                                Log.e(TAG,""+help);


                                                weekStepData2.setProgress(Integer.parseInt(""+help));
                                                textViewWeek2.setText(""+help);
                                                progressAnimator2 = ObjectAnimator.ofInt(weekStepData2,"Progress",0,weekStepData2.getProgress());
                                                progressAnimator2.setDuration(weekStepData2.getProgress());
                                                progressAnimator2.start();
                                                if(Integer.parseInt(""+help) >= newZiel)
                                                {
                                                    weekStepData2.setProgressDrawable(reached);
                                                    progressAnimator2.setDuration(weekStepData2.getProgress());
                                                    progressAnimator2.start();
                                                    mario2.setVisibility(View.VISIBLE);
                                                    mario2.playAnimation();
                                                }
                                            } else {
                                                textViewWeek2.setText("0");
                                                Log.d(TAG, "No such document");
                                            }
                                        } else {
                                            Log.d(TAG, "get failed with ", task.getException());
                                        }
                                    }
                                });

                                try {
                                    calendar.setTime(dateFormat1.parse(date));
                                    calendar.add(Calendar.DAY_OF_MONTH, -6);
                                    DayOne.setText(""+dateFormatday.format(calendar.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e(TAG,""+dateFormat1.format(calendar.getTime()));



                                DocumentReference docRef1 = db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte").document(""+dateFormat1.format(calendar.getTime()));
                                docRef1.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()) {
                                            DocumentSnapshot document = task.getResult();
                                            if (document.exists()) {
                                                String time = ""+document.getData();
                                                String[] date6 = time.split("=");
                                                String help = date6[1];
                                                help = help.substring(0,help.length()-1);

                                                Log.e(TAG,""+help);


                                                weekStepData1.setProgress(Integer.parseInt(""+help));
                                                textViewWeek1.setText(""+help);
                                                progressAnimator1 = ObjectAnimator.ofInt(weekStepData1,"Progress",0,weekStepData1.getProgress());
                                                progressAnimator1.setDuration(weekStepData1.getProgress());
                                                progressAnimator1.start();
                                                if(Integer.parseInt(""+help) >= newZiel)
                                                {
                                                    weekStepData1.setProgressDrawable(reached);
                                                    progressAnimator1.setDuration(weekStepData1.getProgress());
                                                    progressAnimator1.start();
                                                    mario1.setVisibility(View.VISIBLE);
                                                    mario1.playAnimation();
                                                }
                                            } else {
                                                textViewWeek1.setText("0");
                                                Log.d(TAG, "No such document");
                                            }
                                        } else {
                                            Log.d(TAG, "get failed with ", task.getException());
                                        }
                                    }
                                });

                                progressAnimator = ObjectAnimator.ofInt(Stepprogressbar,"Progress",0,Stepprogressbar.getProgress());
                                progressAnimator.setDuration(Stepprogressbar.getProgress());
                                progressAnimator.start();





                                if(total == 0){
                                    total = Integer.parseInt(""+Steps.getText());
                                }

                                    minus = (int) (newZiel - total);

                                    if(minus <= 0) {
                                        int plus = (int)total - newZiel;
                                        beforeCount.setText("WOW! Du bist");
                                        textminusSteps.setText(""+plus);
                                        afterCount.setText("Schritte über deinem Ziel.");
                                        //Goalstepprogress.setProgress(plus);
                                    }else {
                                        textminusSteps.setText("" + minus);
                                    }
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(Dashboard.this, "Schrittanzahl konnte nicht abgerufen werden! Überprüfe deine Internetverbindung.", Toast.LENGTH_LONG).show();
                            }
                        });
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------------


    // ------------------------------------------Legt den Zeitraum der Aktivitäten ( 1 Woche ) fest----------------------------------------------------
    private void showDataSet(DataSet dataSet) {
        Log.e("History", "Data returned for Data type: " + dataSet.getDataType().getName());
        DateFormat dateFormat = DateFormat.getDateInstance();
        DateFormat timeFormat = DateFormat.getTimeInstance();

        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.e("History", "Data point:");
            Log.e("History", "\tType: " + dp.getDataType().getName());
            Log.e("History", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            Log.e("History", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            for(com.google.android.gms.fitness.data.Field field : dp.getDataType().getFields()) {
                Log.e("History", "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));
            }
        }
    }

    //--------------------------------Ziel ändern bzw abfragen--------------------------------------------------------------------------------------

    private void setGoal() {
        db.collection("5AHEL").document("" + currentFirebaseUser.getUid()).collection("Daten")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                newZiel = Integer.parseInt("" + document.get("ziel"));
                            }

                            weekStepData1.setMax(newZiel);
                            weekStepData2.setMax(newZiel);
                            weekStepData3.setMax(newZiel);
                            weekStepData4.setMax(newZiel);
                            weekStepData5.setMax(newZiel);
                            weekStepData6.setMax(newZiel);
                            weekStepData7.setMax(newZiel);

                            Stepprogressbar.setMax(newZiel);



                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                            newZiel = 10000;
                        }
                    }
                });

    }
    // --------------------------------------------------------------------------------------------------------------------------------------------

    // ----------------------------------Ausgabe der Aktivitäten in einer ScrollView----------------------------------------------------------------
    private void dumpSession(Session session) {
        DateFormat dateFormat = getTimeInstance();
        Log.i(TAG, "Data returned for Session: " + session.getName()
                + "\n\tDescription: " + session.getDescription()
                + "\n\tText: " + session.getName()
                + "\n\tStart: " + dateFormat.format(session.getStartTime(TimeUnit.MILLISECONDS))
                + "\n\tEnd: " + dateFormat.format(session.getEndTime(TimeUnit.MILLISECONDS)));


    }
    // ------------------------------------------------------------------------------------------------------------------------------------------------

    private void notification() {
       NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Notify")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Motivity")
                .setContentText("Hast du dein heutiges Ziel schon erreicht?")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

       NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
       notificationManager.notify(1,mBuilder.build());

    }

    // ------------------------------BottomNavigationBar, ist für die Weiterleitung zu dein einzelnen Seiten verantwortlich-----------------------------
    private TextView mTextMessage;

        public static class BottomNavigationViewHelper {
            @SuppressLint("RestrictedApi")
            //deaktiviert dass die Anzeige des ausgewählten Icons hervorgehoben, und die anderen in den Hintergrund gerückt werden
            public static void disableShiftMode(BottomNavigationView view) {
                BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
                try {
                    Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
                    shiftingMode.setAccessible(true);
                    shiftingMode.setBoolean(menuView, false);
                    shiftingMode.setAccessible(true);
                    for (int i = 0; i < menuView.getChildCount(); i++) {
                        BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                        //noinspection RestrictedApi
                        item.setShiftingMode(false);
                        // set once again checked value, so view will be updated
                        //noinspection RestrictedApi
                        item.setChecked(item.getItemData().isChecked());
                    }
                } catch (NoSuchFieldException e) {
                    Log.e("BNVHelper", "Unable to get shift mode field", e);
                } catch (IllegalAccessException e) {
                    Log.e("BNVHelper", "Unable to change value of shift mode", e);
                }
            }
        }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return true;
                case R.id.navigation_dashboard:
                    Intent team = new Intent(Dashboard.this, Team.class);
                    startActivity(team);
                    overridePendingTransition(0, 0);
                    team.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_notifications:
                    Intent badge = new Intent(Dashboard.this, Erfolge.class);
                    startActivity(badge);
                    overridePendingTransition(0, 0);
                    badge.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_setting:
                    Intent setting = new Intent(Dashboard.this, Einstellungen.class);
                    startActivity(setting);
                    overridePendingTransition(0, 0);
                    setting.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
            }
            return false;
        }
    };
    // -----------------------------------------------------------------------------------------------------------------------------------------------
    }