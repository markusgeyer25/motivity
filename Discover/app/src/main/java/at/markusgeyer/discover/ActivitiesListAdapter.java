package at.markusgeyer.discover;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

public class ActivitiesListAdapter extends ArrayAdapter<Activities> {

    private static final String TAG = "StepCounter";
    private Context Context;
    int ressource;

    public ActivitiesListAdapter(Context context, int resource, ArrayList<Activities> objects) {
        super(context, resource, objects);
        Context = context;
        ressource = resource;
    }

    @NonNull
    @Override
    public View getView(int position,View convertView, ViewGroup parent) {
        //get Activity Data
        String activity = getItem(position).getAktivitaet();
        int picture = getItem(position).getPicutre();

        //Acitvity erstellen
        Activities activities = new Activities(activity,picture);

        LayoutInflater inflater = LayoutInflater.from(Context);
        convertView = inflater.inflate(ressource,parent,false);

        TextView activityText = (TextView)convertView.findViewById(R.id.activityListe);
        activityText.setText(activity);
        ImageView activityView = (ImageView) convertView.findViewById(R.id.activityView);
        activityView.setImageResource(picture);

        return convertView;
    }
}
