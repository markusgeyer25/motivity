package at.markusgeyer.discover;

import android.content.Context;
import android.content.Intent;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.SetOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class Add extends AppCompatActivity {

    public static final String TAG = "StepCounter";

    private ImageButton Back;
    private ListView activitylist;

    int index = 0;

    FirebaseFirestore db;
    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();


    FirebaseFirestore firestore = FirebaseFirestore.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        db = FirebaseFirestore.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);



        Back = (ImageButton) findViewById(R.id.Back);
        activitylist = (ListView) findViewById(R.id.activitylist);


        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashboard = new Intent(Add.this, Dashboard.class);
                startActivity(dashboard);
                overridePendingTransition(0,0);
                dashboard.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            }
        });


        //Aktivitäten
        Activities laufen = new Activities("Laufen",R.mipmap.ic_marathon_man);
        Activities walking = new Activities("Spazieren",R.mipmap.ic_one_man_walking);
        Activities hiking = new Activities("Wandern",R.mipmap.ic_walk);
        Activities radfahren = new Activities("Radfahren",R.mipmap.ic_bicycle_rider);
        Activities fußball = new Activities("Fußball",R.mipmap.ic_football);
        Activities krafttraining = new Activities("Krafttraining",R.mipmap.ic_dumbbell);
        Activities zumba = new Activities("Zumba",R.mipmap.ic_stretching_exercises);
        Activities eislaufen = new Activities("Eislaufen",R.mipmap.ic_ice_skating);
        Activities schwimmen = new Activities("Schwimmen",R.mipmap.ic_swimming);
        Activities gardening = new Activities("Gärtnern",R.mipmap.ic_shovel);
        Activities fechten = new Activities("Fechten",R.mipmap.ic_man_with_sword);
        Activities handball = new Activities("Handball",R.mipmap.ic_handball);
        Activities hockey = new Activities("Hockey",R.mipmap.ic_ice_hockey);
        Activities martialarts = new Activities("Kampfkunst",R.mipmap.ic_martial_arts);
        Activities jogging = new Activities("Joggen",R.mipmap.ic_jogging);
        Activities climbing = new Activities("Klettern",R.mipmap.ic_climbing);
        Activities mountainbiking = new Activities("Mountainbiken",R.mipmap.ic_mountainbike);
        Activities paragliding = new Activities("Paragliding",R.mipmap.ic_paragliding);
        Activities reiten = new Activities("Reiten",R.mipmap.ic_horse_riding);
        Activities rudern = new Activities("Rudern",R.mipmap.ic_rudern);
        Activities seilspringen = new Activities("Seilspringen",R.mipmap.ic_jumping_rope);
        Activities skiing = new Activities("Ski fahren",R.mipmap.ic_skiing);
        Activities snowboarding = new Activities("Snowboarden",R.mipmap.ic_snowboarding);
        Activities stepper = new Activities("Stepper",R.mipmap.ic_stepper);
        Activities gotoWork = new Activities("Zur Arbeit gehen",R.mipmap.ic_to_work);
        Activities surfen = new Activities("Surfen",R.mipmap.ic_surfing);
        Activities dancing = new Activities("Tanzen",R.mipmap.ic_dancing);
        Activities scooter = new Activities("Tretroller",R.mipmap.ic_scooter);
        Activities volleyball = new Activities("Volleyball",R.mipmap.ic_volleyball);
        Activities windsurfing = new Activities("Windsurfen",R.mipmap.ic_windsurfing);





        //Aktivität der Liste hinzufügen
        final ArrayList<Activities> activitiesArrayList = new ArrayList<>();
        activitiesArrayList.add(laufen);
        activitiesArrayList.add(walking);
        activitiesArrayList.add(hiking);
        activitiesArrayList.add(radfahren);
        activitiesArrayList.add(fußball);
        activitiesArrayList.add(krafttraining);
        activitiesArrayList.add(zumba);
        activitiesArrayList.add(eislaufen);
        activitiesArrayList.add(schwimmen);
        activitiesArrayList.add(gardening);
        activitiesArrayList.add(fechten);
        activitiesArrayList.add(handball);
        activitiesArrayList.add(hockey);
        activitiesArrayList.add(martialarts);
        activitiesArrayList.add(jogging);
        activitiesArrayList.add(climbing);
        activitiesArrayList.add(mountainbiking);
        activitiesArrayList.add(paragliding);
        activitiesArrayList.add(reiten);
        activitiesArrayList.add(rudern);
        activitiesArrayList.add(seilspringen);
        activitiesArrayList.add(skiing);
        activitiesArrayList.add(snowboarding);
        activitiesArrayList.add(stepper);
        activitiesArrayList.add(gotoWork);
        activitiesArrayList.add(surfen);
        activitiesArrayList.add(dancing);
        activitiesArrayList.add(scooter);
        activitiesArrayList.add(volleyball);
        activitiesArrayList.add(windsurfing);


        activitylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                checkConnetion();
                index = (int) id;

                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss");
                db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Aktivitäten").document(dateFormat.format(calendar.getTime()))
                        .set(activitiesArrayList.get(index),SetOptions.merge());
                SimpleDateFormat dateFormatActivityCollection = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss");
                db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Aktivitäten").document(dateFormat.format(calendar.getTime()))
                        .update("date",dateFormatActivityCollection.format(calendar.getTime()));

                Intent dashboard = new Intent(Add.this, Dashboard.class);
                startActivity(dashboard);
                overridePendingTransition(0,0);
                dashboard.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            }
        });

        ActivitiesListAdapter adapter = new ActivitiesListAdapter(this, R.layout.activity_view,activitiesArrayList);
        activitylist.setAdapter(adapter);

    }

    public void onBackPressed(){
        Intent dashboard = new Intent(Add.this, Dashboard.class);
        startActivity(dashboard);
        overridePendingTransition(0,0);
        dashboard.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    }

    private void checkConnetion(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else {
            Toast.makeText(this, "Überprüfe deine Internetverbindung!", Toast.LENGTH_SHORT).show();
            connected = false;
        }
    }
}
