package at.markusgeyer.discover;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class BrowserView extends AppCompatActivity {

    public WebView myWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        myWebView = (WebView) findViewById(R.id.browserView);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.loadUrl("http://checklsmw.selfhost.eu/subhompages/elaadmin/impressum.html");
    }
}
