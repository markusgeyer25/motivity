package at.markusgeyer.discover;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class ChangeGoal extends AppCompatActivity {


    private FirebaseAuth firebaseAuth;
    private FirebaseFirestore db;
    private FirebaseUser currentFirebaseUser;
    private FirebaseFirestore firestore = FirebaseFirestore.getInstance();


    ImageButton back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_goal);

        db = FirebaseFirestore.getInstance();
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        final TextView ChangeStep = (TextView) findViewById(R.id.editStepGoal);
        Button editGoal = (Button) findViewById(R.id.buttonEdit);
        back = (ImageButton) findViewById(R.id.Back);

        editGoal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeStep.getText();

                Toast.makeText(ChangeGoal.this, "Ziel erfolgreich geändert!", Toast.LENGTH_SHORT).show();

                Map<String, Object> goal = new HashMap<>();
                goal.put("ziel", ""+ChangeStep.getText());

                db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Daten").document("goal")
                        .set(goal);

                Intent a = new Intent(ChangeGoal.this, Dashboard.class);
                startActivity(a);
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent dashboard = new Intent(ChangeGoal.this, Dashboard.class);
                startActivity(dashboard);
                overridePendingTransition(0,0);
                dashboard.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            }
        });

    }
    public void onBackPressed(){
            Intent a = new Intent(ChangeGoal.this, Dashboard.class);
            startActivity(a);
            overridePendingTransition(0, 0);
            a.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    }
}
