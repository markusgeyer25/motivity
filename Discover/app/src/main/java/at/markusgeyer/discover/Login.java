package at.markusgeyer.discover;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

/*
    Markus Geyer am 14.08.2018
 */
public class Login extends AppCompatActivity implements View.OnClickListener{


    private Button buttonLogin;
    private EditText editLoginEmail;
    private EditText editLoginPasswort;

    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null){
            //user eingeloggt -> Dashboard
            finish();
            Intent setting = new Intent(Login.this, Dashboard.class);
            startActivity(setting);
            overridePendingTransition(0, 0);
            setting.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

        }

        editLoginEmail = (EditText) findViewById(R.id.editLoginEmail);
        editLoginPasswort = (EditText) findViewById(R.id.editLoginPasswort);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        progressDialog = new ProgressDialog(this);

        buttonLogin.setOnClickListener(this);
        checkConnetion();
    }

    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);

    }

    private void checkConnetion(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            connected = true;
        }
        else {
            Toast.makeText(this, "Überprüfe deine Internetverbindung!", Toast.LENGTH_SHORT).show();
            connected = false;
        }
    }



    private void userLogin(){
        String email = editLoginEmail.getText().toString().trim();
        String pass = editLoginPasswort.getText().toString().trim();
        //schauen ob Textfelder leer sind?
        if(TextUtils.isEmpty(email)){
            Toast.makeText(this,"Geben Sie eine Emailadresse ein!",Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(pass)){
            Toast.makeText(this,"Geben Sie ein Passwort ein!",Toast.LENGTH_LONG).show();
            return;
        }
        //wenn Textfelder befüllt sind
        progressDialog.setMessage("Anmeldung läuft ...");
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (!task.isSuccessful()) {
                            Log.e("ERROR", "onComplete: Failed=" + task.getException().getMessage());
                        }
                        if(task.isSuccessful()){
                            //Dashboard oeffnen
                            finish();
                            startActivity(new Intent(getApplicationContext(), Dashboard.class));
                        }
                        else{
                            Toast.makeText(Login.this, "Anmeldedaten sind ungültig!", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(Login.this ,Login.class));
                            return;
                        }
                    }
                });


    }
    @Override
    public void onClick(View view) {
        if(view == buttonLogin){
            checkConnetion();
            userLogin();
        }
    }
}
