package at.markusgeyer.discover;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Picture;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.net.Uri;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
/*
    Markus Geyer am 14.08.2018
 */
public class Einstellungen extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth firebaseAuth;
    private GoogleApiClient mGoogleApiClient;
    private FirebaseUser currentFirebaseUser;
    private FirebaseStorage storage = FirebaseStorage.getInstance();

    public static Activity activity;


    private ImageView Pic;
    private TextView usermail;
    private Button buttonLogout, btnLizenzen, btnHilfe, btnImpressum;
    private ImageButton updatePic;



    private static int RESULT_LOAD_IMG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_einstellungen);

        activity = this;

        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth.getCurrentUser() == null) {
            finish();
            startActivity(new Intent(this, Login.class));
        }

        FirebaseUser user = firebaseAuth.getCurrentUser();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_setting);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);

        usermail = (TextView) findViewById(R.id.usermail);
        usermail.setText(user.getEmail());

        btnLizenzen = (Button) findViewById(R.id.buttonLizenzen);
        btnHilfe = (Button) findViewById(R.id.buttonHilfe);
        btnImpressum = (Button) findViewById(R.id.buttonImpressum);
        Pic = (ImageView)findViewById(R.id.imageView);

        updatePic = (ImageButton)findViewById(R.id.imageButton);
        updatePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent getImageIntent = new Intent(Intent.ACTION_PICK);
                getImageIntent .setType("image/*");
                startActivityForResult(getImageIntent , RESULT_LOAD_IMG );
            }
        });
        //Firebase Profilbild bekommen
        StorageReference imagedata = storage.getReference("firememes/"+currentFirebaseUser.getUid()+".png");
        final long ONE_MEGABYTE = 1024 * 1024;
        imagedata.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                Pic.setImageBitmap(bitmap);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });

        buttonLogout = (Button) findViewById(R.id.buttonLogout);
        buttonLogout.setOnClickListener(this);


        btnLizenzen.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                String url = "http://checklsmw.selfhost.eu/#/lizenzen";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(getColor(R.color.colorPrimary));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(Einstellungen.this, Uri.parse(url));
            }
        });

        btnHilfe.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                String url = "http://checklsmw.selfhost.eu/#/faq";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(getColor(R.color.colorPrimary));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(Einstellungen.this, Uri.parse(url));
            }
        });

        btnImpressum.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                String url = "http://checklsmw.selfhost.eu/#/impressum";
                CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
                builder.setToolbarColor(getColor(R.color.colorPrimary));
                CustomTabsIntent customTabsIntent = builder.build();
                customTabsIntent.launchUrl(Einstellungen.this, Uri.parse(url));
            }
        });

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode== RESULT_LOAD_IMG  && resultCode == RESULT_OK) {
            Uri fullPhotoUri = data.getData();
            Pic.setImageURI(fullPhotoUri);

            Pic.setDrawingCacheEnabled(true);
            Pic.buildDrawingCache();
            Bitmap bitmap = Pic.getDrawingCache();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100,baos);
            Pic.setDrawingCacheEnabled(false);
            byte[] dataP = baos.toByteArray();

            //Bild Firebase hochladen
            final String path = "firememes/" + currentFirebaseUser.getUid()+".png";
            final StorageReference firememRef = storage.getReference(path);
            UploadTask uploadTask = firememRef.putBytes(dataP);
        }
    }
    // ---------------------------------------Verhindert das Zurück klicken, da es keine Subpages gibt-------------------------------------------------------
    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }
    // -------------------------------------------------------------------------------------------------------------------------------------------------------

    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    public static class BottomNavigationViewHelper {
        @SuppressLint("RestrictedApi")
        public static void disableShiftMode(BottomNavigationView view) {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
            try {
                Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
                shiftingMode.setAccessible(true);
                shiftingMode.setBoolean(menuView, false);
                shiftingMode.setAccessible(true);
                for (int i = 0; i < menuView.getChildCount(); i++) {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                    //noinspection RestrictedApi
                    item.setShiftingMode(false);
                    // set once again checked value, so view will be updated
                    //noinspection RestrictedApi
                    item.setChecked(item.getItemData().isChecked());
                }
            } catch (NoSuchFieldException e) {
                Log.e("BNVHelper", "Unable to get shift mode field", e);
            } catch (IllegalAccessException e) {
                Log.e("BNVHelper", "Unable to change value of shift mode", e);
            }
        }
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent home = new Intent(Einstellungen.this, Dashboard.class);
                    startActivity(home);
                    overridePendingTransition(0, 0);
                    home.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_dashboard:
                    Intent team = new Intent(Einstellungen.this, Team.class);
                    startActivity(team);
                    overridePendingTransition(0, 0);
                    team.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_notifications:
                    Intent badge = new Intent(Einstellungen.this, Erfolge.class);
                    startActivity(badge);
                    overridePendingTransition(0, 0);
                    badge.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_setting:
                    return true;
            }
            return false;
        }
    };


    @Override
    public void onClick(View view) {
        if (view == buttonLogout) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            // ...
                            //Toast.makeText(getApplicationContext(),"Logged Out", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            firebaseAuth.signOut();
                            startActivity(i);
                        }
                    });
        }
    }

}
