package at.markusgeyer.discover;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/*
    Markus Geyer am 14.08.2018
 */
public class Erfolge extends AppCompatActivity {

    public static final String TAG = "StepCounter";

    int step = 0;
    int end = 0;

    private FirebaseAuth firebaseAuth;
    private ObjectAnimator progressAnimator;
    private FirebaseFirestore db;
    private FirebaseFirestore firestore = FirebaseFirestore.getInstance();
    private FirebaseUser currentFirebaseUser;

    private ImageView tenK;
    private TextView ten,tenStep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_erfolge);

        final ProgressBar stepprogress = (ProgressBar)findViewById(R.id.stepstogoal);
        final TextView currentSteps = (TextView)findViewById(R.id.currentsteps);

        tenK = (ImageView) findViewById(R.id.imageView3);
        ten = (TextView)findViewById(R.id.firsterfolg);
        tenStep = (TextView)findViewById(R.id.firsterfolgtext);

        firebaseAuth = FirebaseAuth.getInstance();

        if(firebaseAuth.getCurrentUser() != null){
        }
        else{
            Intent login = new Intent(Erfolge.this, Login.class);
            startActivity(login);
            overridePendingTransition(0, 0);
            login.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        }

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setSelectedItemId(R.id.navigation_notifications);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        Einstellungen.BottomNavigationViewHelper.disableShiftMode(navigation);

        //------------------------------------------Firebase Firestore-------------------------------------------------------------------------------
        db = FirebaseFirestore.getInstance();
        currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setTimestampsInSnapshotsEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        final Map<String,Object> zehntausend = new HashMap<>();


        db.collection("5AHEL").document(""+currentFirebaseUser.getUid()).collection("Schritte")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @SuppressLint("ResourceAsColor")
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

                                    step =  Integer.parseInt("" + document.get("schritte"));
                                    end = step+end;
                                    stepprogress.setProgress(end);
                                    currentSteps.setText(""+end);

                            }
                            if(end >= 10000) {
                                zehntausend.put("reached", true);
                                db.collection("5AHEL").document("" + currentFirebaseUser.getUid()).collection("Erfolge").document("10000").set(zehntausend);
                                tenK.setImageResource(R.mipmap.iz_winner);
                                ten.setTextColor(Erfolge.this.getResources().getColor(R.color.colorBlack));
                                tenStep.setTextColor(Erfolge.this.getResources().getColor(R.color.colorBlack));
                            }else if(end <= 9999){
                                zehntausend.put("reached", false);
                                db.collection("5AHEL").document("" + currentFirebaseUser.getUid()).collection("Erfolge").document("10000").set(zehntausend);
                                tenK.setImageResource(R.mipmap.iz_winner0);
                                ten.setTextColor(Erfolge.this.getResources().getColor(R.color.colorGrey));
                                tenStep.setTextColor(Erfolge.this.getResources().getColor(R.color.colorGrey));
                            }
                            stepprogress.setProgress(end/100);
                            stepprogress.setMax(1000);
                            //stepprogress.setProgress(end/100);
                            progressAnimator = ObjectAnimator.ofInt(stepprogress,"Progress",1,stepprogress.getProgress());
                            progressAnimator.setDuration(stepprogress.getProgress());
                            progressAnimator.start();

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public void onBackPressed(){
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);

    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    Intent home = new Intent(Erfolge.this, Dashboard.class);
                    startActivity(home);
                    overridePendingTransition(0,0);
                    home.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_dashboard:
                    Intent team = new Intent(Erfolge.this, Team.class);
                    startActivity(team);
                    overridePendingTransition(0,0);
                    team.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
                case R.id.navigation_notifications:
                    return true;
                case R.id.navigation_setting:
                    Intent setting = new Intent(Erfolge.this, Einstellungen.class);
                    startActivity(setting);
                    overridePendingTransition(0,0);
                    setting.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    return true;
            }
            return false;
        }
    };

}
